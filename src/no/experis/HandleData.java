package no.experis;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

public class HandleData {

    private Request animeData;

    HandleData() {
        this.animeData = new Request();
    }

    public String[] pullData(String day) {
        JSONObject data = null;
        try {
            data = animeData.requestURL(day);
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            if(data != null) {
                ArrayList<String> tmp = new ArrayList<String>();
                JSONArray jArr = new JSONArray(data.get(day).toString());
                for(int i = 0; i < jArr.length(); i++){
                    JSONObject o = jArr.getJSONObject(i);
                    tmp.add(o.get("title").toString());
                    tmp.add(o.get("airing_start").toString());
                }
                return tmp.stream().toArray(String[]::new);
            }
            return null;
        }
    }
}
