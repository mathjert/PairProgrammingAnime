package no.experis;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        //String day = "monday";
        HandleData test = new HandleData();

        UserInteraction ui = new UserInteraction();
        //ui.dataOutput(test.pullData(day));
        while(true){
            String input = ui.getDayFromUser();
            if(input != null){
                ui.dataOutput(test.pullData(input));
            }else {
                System.out.println("Non valid input. Write the day of week\n");
            }

        }
    }
}
class UserInteraction {

    public String getDayFromUser() {
        Scanner in = new Scanner(System.in);
        System.out.print("Which day of the week do you want to see anime air times for?\n:>");
        String input = in.nextLine();
        if(input.toLowerCase().contains("monday"))
            return "monday";
        if(input.toLowerCase().contains("tuesday"))
            return "tuesday";
        if(input.toLowerCase().contains("wednesday"))
            return "wednesday";
        if(input.toLowerCase().contains("thursday"))
            return "thursday";
        if(input.toLowerCase().contains("friday"))
            return "friday";
        if(input.toLowerCase().contains("saturday"))
            return "saturday";
        if(input.toLowerCase().contains("sunday"))
            return "sunday";

        return null;
    }

    public void dataOutput(String[] data) {
        StringBuilder prompt = new StringBuilder();
        int count = 1;
        for (String s: data) {
            if (count % 2 != 0) {
                prompt.append("Title:\t").append(s).append("\t\t");
            } else {
                String tmp[] = s.split("T|\\+");
                prompt.append("Air Start Time:\t").append(tmp[1]).append("\n\n");
            }
            count++;
        }
        System.out.println(prompt.toString());
    }
}
