Anime search 
====
This program allows the user to see the scheduled Anime for a day of the current week.   
This task was a pair programming task, given by Dean Von Shoultz

#### About
Lets user search for air times of anime for a given day of this week. This solutution is "Grandma safe" 
which means you can write whatever you want as long as you have the desired day in your input.
#### Pair Programming 
For this task we were tasked to use one of the techniques described the document [*What's the Best Way to Pair?*]( https://builttoadapt.io/whats-the-best-way-to-pair-a8699f9beb81) by Pivotal.
We chose to use the *Task List* technique, which seemed the most appropriate for the scope of the task and our 
individual working preferences.  
We wrote one of the tasks to completion before switching. We did this on a single computer, so it was somewhat of a 
merging of two techniques *Task List* and *Pairmate/One-keyboard*.  
##### Experience 
We thought it was a smart way of team building. This made it so we got to know another member of the Experis Academy
Team, which is good for the future of the team knowledge sharing wise. It was also a good exercise before going into
the case period, as it gives experience with working as a part of a team instead of doing everything on your own. 
###### Pros
- Team Building
- Knowledge Sharing
- Being put outside your comfort zone
- Fast review of differentiating opinions for approaches 
- Good for learning new techniques for approaches to code
- Real time code reviews
###### Cons
- Faster to split the work and work separate
- More payoff with larger projects, doesn't seem as valuable with small tasks that can be 
completed within an hour or two. 
##### Task List Used 
 - [X] Establish connection
 - [X] Pull data from API
 - [X] Get return in JSONObject
 - [X] Deserialize JSON
 - [X] Print wanted data
 - [X] Get user input
 - [X] Handle user input 
#### Usage examples
If one wants to use the code in without the commandline, this is how you use it
```java
 // Make object of HandleData
 HandleData hd = new HandleData();

 // Run pulldata on selected day
 String[] data = hd.pullData("monday");
```
#### Tools used
 - json.org's JSONObject and JSONArray to deserialize JSON
 - jikan Anime API
 - java.net.URLConnection
 - java.net.URL
 
 ###### Authors
 Sadegh Hosseinpoor  
 Mathias Christian Hjertholm